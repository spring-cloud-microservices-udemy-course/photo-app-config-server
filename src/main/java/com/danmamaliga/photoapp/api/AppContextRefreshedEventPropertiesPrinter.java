package com.danmamaliga.photoapp.api;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.stereotype.Component;

// Enable when needed
//@Component
public class AppContextRefreshedEventPropertiesPrinter {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@EventListener
	public void handleContextRefreshed(ContextRefreshedEvent event) {
		ConfigurableEnvironment env = (ConfigurableEnvironment) event.getApplicationContext().getEnvironment();
		log.info("Properties start...");
		env.getPropertySources().stream().filter(MapPropertySource.class::isInstance)
				.map(ps -> ((MapPropertySource) ps).getSource().keySet()).flatMap(Collection::stream).distinct()
				.sorted().forEach(key -> log.info("{}={}", key, env.getProperty(key)));
		log.info("Properties end");
	}

}
