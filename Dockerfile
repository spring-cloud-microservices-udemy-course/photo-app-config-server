FROM eclipse-temurin:21
VOLUME /tmp
COPY /src/main/resources/apiEncryptionKey.jks apiEncryptionKey.jks
COPY target/PhotoAppConfigServer-0.0.1-SNAPSHOT.jar ConfigServer.jar
ENTRYPOINT ["java","-jar","ConfigServer.jar"]